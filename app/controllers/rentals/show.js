import Ember from 'ember';

export default Ember.Controller.extend({
    showInput: null,
    init() {
        this._super(...arguments);
        this.comments = this.get('store').query('comment', {});
        //this.comments = this.store.findAll('comment');
    },
    actions: {
        toggleReplyInput(index) {
            if (this.get('showInput', index) && !index) {
                this.set('showInput', null);
            } else {
                this.set('showInput', index);
            }
        },
        postComment(message, parent) {
            let saveComment = (comment) => {
                comment.save()
                    .then((data) => {

                        this.set('comments', this.get('store').query('comment', {}));
                        this.set('message', '');
                        this.set('replyMessage', '');
                        this.set('showInput', null);
                    }); // => POST to 'api/comment'
            }

            if (parent && parent.id) {
                this.get('store')
                    .find('comment', parent.id)
                    .then((comment) => {
                        let children = comment.get('children');

                        children.push({
                            message: message
                        });

                        comment.set('children', children);
                        saveComment(comment);
                    });
            } else {
                let comment = this.store.createRecord('comment', {
                    message: message,
                    parent: null,
                });
                saveComment(comment);
            }

        }
    }
});
