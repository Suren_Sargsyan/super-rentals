import Ember from 'ember';

export default Ember.Component.extend({
    classNames: ['list-filter'],
    value: '',

    init() {
        this._super(...arguments);
        this.get('filter')('').then((res) => {
            this.set('results', res);
        });
    },

    actions: {
        handleFilterEntry() {
            let filterInputValue = this.get('value'),
                filterAction = this.get('filter');

            filterAction(filterInputValue).then((filterRes) => {
                this.set('results', filterRes);
            });
        }
    }
});
