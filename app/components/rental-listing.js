import Ember from 'ember';

export default Ember.Component.extend({
    isWide: false,
    init() {
        this._super(...arguments);
    },

    actions: {
        toggleImageSize() {
            this.toggleProperty('isWide');
        }
    }
});
