import DS from 'ember-data';

export default DS.Model.extend({
    //rental_id: DS.attr(),
    message: DS.attr(),
    children: DS.attr('array'),
});
