# super-rentals



## Installation

* `git clone <repository-url>` this repository
* `cd super-rentals`
* `npm install`
* `bower install`

## Running / Development

* `ember serve`
* Visit your app at [http://localhost:4200](http://localhost:4200).
