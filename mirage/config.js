export default function () {
    this.namespace = '/api';
    let comments = [{
            type: 'comments',
            id: 0,
            attributes: {
                message: 'Eome message',
                children: [{
                    message: 'test test test'
                }],
            }
        },
        {
            type: 'comments',
            id: 1,
            attributes: {
                message: 'some oajdchb',
                children: [],
            }
        },
        {
            type: 'comments',
            id: 2,
            attributes: {
                message: 'test replyed comment',
                children: [],
            }
        }
    ];

    this.get('/comments', (db, request) => {

        if (request.queryParams.parent) {
            let filteredComments = comments.filter((i) => {
                return i.attributes.parent.toLowerCase().indexOf(request.queryParams.parent.toLowerCase()) !== -1;
            });

            return {
                data: filteredComments
            };
        }

        let filteredComments = comments.filter((i) => {
            if (!i.attributes.parent) {
                return i;
            }
        });

        return {
            data: filteredComments
        };
    });

    this.post('/comments', (db, request) => {
        let comment = JSON.parse(request.requestBody).data;
        comment.id = comments.length + 1;
        comments.push(comment);

        console.log(comment);
        return {
            data: comment,
        };
    });

    this.patch('/comments/:id', (db, request) => {
        let updatedComment = JSON.parse(request.requestBody).data;

        comments = comments.map((comment) => {
            if (request.params.id == comment.id) {
                console.log(11);
                return updatedComment;
            }else {
                console.log(22);
                return comment;
            }
        });

        return {
            data: updatedComment,
        };
    });


    this.get('/comments/:id', (db, request) => {
        let res = {
            data: comments.find((comment) => {
                return request.params.id == comment.id
            })
        };

        return res;
    });

    let rentals = [{
            type: 'rentals',
            id: 'grand-old-mansion',
            attributes: {
                title: "Grand Old Mansion",
                owner: "Veruca Salt",
                city: "San Francisco",
                type: "Estate",
                bedrooms: 15,
                image: "https://upload.wikimedia.org/wikipedia/commons/c/cb/Crane_estate_(5).jpg",
                description: "This grand old mansion sits on over 100 acres of rolling hills and dense redwood forests."
            }
        },
        {
            type: 'rentals',
            id: 'urban-living',
            attributes: {
                title: "Urban Living",
                owner: "Mike Teavee",
                city: "Seattle",
                type: "Condo",
                bedrooms: 1,
                image: "https://upload.wikimedia.org/wikipedia/commons/0/0e/Alfonso_13_Highrise_Tegucigalpa.jpg",
                description: "A commuters dream. This rental is within walking distance of 2 bus stops and the Metro."
            }
        },
        {
            type: 'rentals',
            id: 'downtown-charm',
            attributes: {
                title: "Downtown Charm",
                owner: "Violet Beauregarde",
                city: "Portland",
                type: "Apartment",
                bedrooms: 3,
                image: "https://upload.wikimedia.org/wikipedia/commons/f/f7/Wheeldon_Apartment_Building_-_Portland_Oregon.jpg",
                description: "Convenience is at your doorstep with this charming downtown rental. Great restaurants and active night life are within a few feet."
            }
        }
    ];

    this.get('/rentals', function (db, request) {
        if (request.queryParams.city !== undefined) {
            let filteredRentals = rentals.filter(function (i) {
                return i.attributes.city.toLowerCase().indexOf(request.queryParams.city.toLowerCase()) !== -1;
            });
            return {
                data: filteredRentals
            };
        } else {
            return {
                data: rentals
            };
        }
    });

    // Find and return the provided rental from our rental list above
    this.get('/rentals/:id', (db, request) => {
        let res = {
            data: rentals.find((rental) => request.params.id === rental.id)
        };

        return res;
    });

    // Find and return the provided rental from our rental list above
    this.post('/rental', (db, request) => {
        console.log(arguments);
        return {
            res: 'test'
        };
    });
}
